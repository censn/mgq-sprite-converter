# Monster Girl Quest! Sprite converter #
Converts MGQ's sprites into a more usable format.

Download executable:
https://nofile.io/f/Q5mMgKnNwjW/MGQ+Sprite+Converter+1.1.7z

New in 1.1: Artifacts should be less visible.

For more information, check out:
http://monstergirlquest.wikia.com/wiki/User_blog:Tamamoball/Extracting_assets_(CG)_from_arc.nsa_files