﻿using B83.Image.BMP;
using System.Collections;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ConversionManager : MonoBehaviour {
    private static readonly Color TRANSPARENT_COLOR = Color.green;
    private const int SECONDS_SHOWING_FINISHED = 1;
    private const string ACCEPTED_EXTENSION = ".bmp";
    private static readonly string INPUT_FOLDER_NAME = "Input";
    private static readonly string OUTPUT_FOLDER_NAME = "Output";

    [SerializeField]
    private Text instructions;

    [SerializeField]
    private Button convert;

    [SerializeField]
    private Text buttonText;

    [SerializeField]
    private Image progressBar;

    private DirectoryInfo input;
    private DirectoryInfo output;
    private BMPLoader bmpLoader = new BMPLoader();
    private bool isConverting;

    private int NumberOfEligibleFiles {
        get {
            return input.GetFiles()
                .Count(info => IsFileBMP(info));
        }
    }

    public void Convert() {
        isConverting = true;
        convert.interactable = false;
        StartCoroutine(ConvertFiles());
    }

    private bool IsFileBMP(FileInfo info) {
        return Path.GetExtension(info.FullName).ToLower().Equals(ACCEPTED_EXTENSION);
    }

    private void Start() {
        input = GetDirectory(Directory.GetCurrentDirectory() + "/" + INPUT_FOLDER_NAME);
        output = GetDirectory(Directory.GetCurrentDirectory() + "/" + OUTPUT_FOLDER_NAME);
        instructions.text = string.Format(
            "Move raw sprites of type .bmp into the <b>{0}</b> folder and press Convert. Processed files will be in the <b>{1}</b> folder. " +
            "These folders are in the program's directory.",
            INPUT_FOLDER_NAME,
            OUTPUT_FOLDER_NAME
            );
    }

    private void Update() {
        int fileCount = NumberOfEligibleFiles;
        if (!isConverting) {
            buttonText.text = string.Format("Convert <color=cyan>{0}</color> sprites", fileCount);
        }
        convert.interactable = !isConverting && fileCount > 0;
        progressBar.enabled = isConverting;
    }

    // Converts files and gives progress notifications
    private IEnumerator ConvertFiles() {
        progressBar.color = Color.green;
        FileInfo[] infos = input.GetFiles();

        int count = 0;
        foreach (FileInfo info in infos) {
            if (IsFileBMP(info)) {
                buttonText.text = string.Format("{1}/{2} - {0}", info.Name, ++count, infos.Length); // update button text
                MergeToPNG(info);
                progressBar.fillAmount = count / (float) infos.Length; // update progress bar
                yield return null;
            }
        }
        progressBar.color = Color.cyan;
        buttonText.text = "Done!";
        yield return new WaitForSeconds(SECONDS_SHOWING_FINISHED);
        isConverting = false;
    }

    private void MergeToPNG(FileInfo bmp) {
        Texture2D raw = bmpLoader.LoadBMP(bmp.FullName).ToTexture2D();

        int spriteWidth = raw.width / 2; // halving the image gives the output width
        int spriteHeight = raw.height; // height is still the same

        Texture2D texture = new Texture2D(spriteWidth, spriteHeight, TextureFormat.RGBA32, false);

        // write each pixel on the output sprite
        for (int i = 0; i < spriteWidth; i++) {
            for (int j = 0; j < spriteHeight; j++) {
                Color primary = raw.GetPixel(i, j); // The sprite on the left with all the colors
                Color mask = raw.GetPixel(i + spriteWidth, j); // Mask's pixel is half a width away from primary

                Color calculated = Color.clear;

                if (!primary.Equals(TRANSPARENT_COLOR)) {
                    float alpha = 1f - mask.grayscale; // if mask's pixel is black, primary's pixel should be fully visible
                    calculated = new Color(primary.r, primary.g, primary.b, alpha);
                }
                texture.SetPixel(i, j, calculated);
            }
        }
        texture.Apply();
        byte[] binary = texture.EncodeToPNG();

        // Write the file
        File.WriteAllBytes(output.FullName + "/" + bmp.Name, binary);
    }

    // Creates directory if it does not exist
    private DirectoryInfo GetDirectory(string name) {
        if (!Directory.Exists(name)) {
            Directory.CreateDirectory(name);
        }
        return new DirectoryInfo(name);
    }
}
